module github.com/wanmei002/goutil

go 1.16

require (
	github.com/gomodule/redigo v1.8.5 // indirect
	google.golang.org/grpc v1.39.0
)
